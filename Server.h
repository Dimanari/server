#pragma once
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>
#include <thread>
#include <string>


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

struct client_type
{
	int id;
	SOCKET socket;
	std::string mymsg;
};

const char OPTION_VALUE = 1;
const int MAX_CLIENTS = 10;
class Server
{
	static int process_client(client_type &new_client, std::vector<client_type> &client_array, std::thread &thread);
	static int parseHTTP(const char* toParse, std::string& msg);
    static int fileType(std::string str);
    static void Get(std::string& msg, std::string path, std::string type);
    static void Head(std::string& msg, std::string path, std::string type);
    static void BadReq(std::string& msg, std::string path, std::string type);
    static void Options(std::string& msg, std::string path, std::string type);
public:
	static int main();
};

template <class Container>
void split3(const std::string& str, Container& cont,
    char delim = '?')
{
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}

void cleanurl(char* str);

void cleanurl(std::string& str);