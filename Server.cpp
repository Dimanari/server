#include "Server.h"
#include <iostream>
#include <sstream>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

int Server::process_client(client_type & new_client, std::vector<client_type>& client_array, std::thread & thread)
{
	std::string msg = "";
	char tempmsg[DEFAULT_BUFLEN] = "";
	bool keep = true;
	//Session
	while (keep)
	{
		memset(tempmsg, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{
			int iResult = recv(new_client.socket, tempmsg, DEFAULT_BUFLEN, 0);

			
			if (iResult != SOCKET_ERROR)
			{
				if (strcmp("", tempmsg))
					msg = "Client #" + std::to_string(new_client.id) + ": " + tempmsg;

				std::cout << msg.c_str() << std::endl << std::endl << std::endl;
				std::string S(tempmsg);
				new_client.mymsg += S.substr(0, DEFAULT_BUFLEN);

				if (iResult < DEFAULT_BUFLEN)
				{
					msg = new_client.mymsg;
					std::cout << msg << std::endl << std::endl << std::endl;
					parseHTTP(msg.c_str() , new_client.mymsg);

					send(new_client.socket, new_client.mymsg.c_str(), new_client.mymsg.size(), 0);
					
					std::cout << new_client.mymsg << std::endl << std::endl << std::endl;

					new_client.mymsg = "";
					keep = false;
					if (!keep)
					{
						closesocket(new_client.socket);
						closesocket(client_array[new_client.id].socket);
						client_array[new_client.id].socket = INVALID_SOCKET;
					}
				}
			}
			else
			{
				msg = "Client #" + std::to_string(new_client.id) + " Disconnected";

				std::cout << msg << std::endl << std::endl << std::endl;

				closesocket(new_client.socket);
				closesocket(client_array[new_client.id].socket);
				client_array[new_client.id].socket = INVALID_SOCKET;

				break;
			}
		}
	} //end while

	thread.detach();

	return 0;
}

int Server::parseHTTP(const char* toParse, std::string& msg)
{
	std::istringstream str(toParse);
	int returnval = 0;
	std::string line;
	std::vector<std::string> lines;
	std::string line1;
	std::getline(str, line1);
	lines.push_back(line1);
	while (std::getline(str, line)) {
		lines.push_back(line);
		if (line.find("Connection:") != std::string::npos)
		{
			if ((line.find("keep-alive") != std::string::npos))
			{
				returnval = 1;
			}
		}
	}

	std::string type, path;

	std::istringstream command(line1.c_str());
	command >> type >> path;
	cleanurl(path);
	if (type == "GET")
	{
		Get(msg, path, type);
	}
	else if (type == "POST")
	{
		int i;
		for (i = 0; i < lines.size(); i++)
			if (lines[i].size() < 2)
				break;
		std::string postmsg = "";
		for (i++; i < lines.size(); i++)
		{
			postmsg += lines[i];
		}
		std::vector<std::string> data;
		std::string filemsg;
		split3(postmsg, data, '&');

		//simple WEB servers don't have a proper way to handle the data of a post messages and instead treat it the same way they would a GET.
		Get(msg, path, type);
	}
	else if (type == "HEAD")
	{
		Head(msg, path, type);
	}
	else if (type == "OPTIONS")
	{
		Options(msg, path, type);
	}
	else
	{
		BadReq(msg, path, type);
	}

	return returnval;
}

int Server::fileType(std::string str)
{
	if (str.find("htm") != std::string::npos)
		return 0;
	if (str.find("jpg") != std::string::npos || str.find("jpeg") != std::string::npos)
		return 1;
	if (str.find("gif") != std::string::npos)
		return 2;
	if (str.find("css") != std::string::npos)
		return 3;
		//css
	return -1;
}

void Server::Get(std::string& msg, std::string path, std::string type)
{
	std::vector<std::string> path_sub;
	split3(path, path_sub);
	std::string filemsg;
	//try to locate the file
	FILE* pFile = 0;

	while (path_sub.size() < 2)//path and arguments are needed
	{
		path_sub.push_back("");//if doesn't exist add an empty string
	}
	if (path_sub[0].size() < 2) // valid paths always starts with / therefore the smallest viable path size is 2
		path_sub[0] = "/index.htm"; // default path

	path_sub[0] = "www" + path_sub[0]; //default directory(avoid people messing with your server software)

	int errorNo = (fopen_s(&pFile, path_sub[0].c_str(), "rb")); //read binary so images and other files can be loaded as well
	//if file found send file
	long long sizeOfFile = 0;
	if (errorNo == 0)
	{
		msg = "HTTP/1.1 200 OK\n";

		int _type;
		std::vector<std::string> path_sub2;
		split3(path_sub[0], path_sub2, '.');//split to get the file suffix
		msg += "Content-Type: ";
		switch (_type = fileType(path_sub2[path_sub2.size() - 1]))
		{
		case 0:
			msg += "text/html \r\n";
			break;
		case 1:
			msg += "image/jpeg \r\n";
			break;
		case 2:
			msg += "image/gif \r\n";
			break;
		case 3:
			msg += "text/css \r\n";
			break;
		default:
			msg += path_sub2[path_sub2.size() - 1]; // just state the file suffix in case you are clueless and the client isn't
			msg += " \r\n";
		}
		int c;
		while ((c = fgetc(pFile)) != EOF) {
			char _c = c; // cast to char(could've been done in the assignment but it doesn't matter)
			filemsg += _c;
			sizeOfFile++;
		}
		fclose(pFile);
		if (filemsg.size() != sizeOfFile)
			std::cout << "File reading caused corruption in the string" << std::endl;
	}
	//if file not found
	else
	{
		msg = "HTTP/1.1 404 Not Found\n";
		while (path_sub.size() < 2)
		{
			path_sub.push_back("");
		}
		if (path_sub[0].size() < 2)
			path_sub[0] = "/index.htm";

		path_sub[0] = "www" + path_sub[0];
		int _type;
		msg += "Content-Type: ";
		msg += "text/html \r\n";
		filemsg += "<html><head><title>Missing File</title></head><body>";
		filemsg += "Request Type: ";
		filemsg += type;
		filemsg += "<br>Path: ";
		filemsg += path_sub[0];
		filemsg += "<br>subdata: ";
		filemsg += path_sub[1];
		filemsg += "</body></html>";
	}
	msg += "Content-Length: ";
	if (sizeOfFile > 0)
		msg += std::to_string(sizeOfFile);
	else
		msg += std::to_string(filemsg.size());
	msg += "\r\n";

	//end header
	msg += "\r\n";
	for (int i = 0; i < filemsg.size(); i++)
		msg += filemsg.c_str()[i];
}

void Server::Head(std::string& msg, std::string path, std::string type)
{
	std::vector<std::string> path_sub;
	split3(path, path_sub);
	std::string filemsg;
	//try to locate the file
	FILE* pFile = 0;
	int errorNo = (fopen_s(&pFile, path_sub[0].c_str(), "rb"));
	//if file found send file
	long long sizeOfFile = 0;
	if (errorNo == 0)
	{
		msg = "HTTP/1.1 200 OK\n";
		while (path_sub.size() < 2)
		{
			path_sub.push_back("");
		}
		if (path_sub[0].size() < 2)
			path_sub[0] = "/index.htm";

		path_sub[0] = "www" + path_sub[0];
		int _type;
		std::vector<std::string> path_sub2;
		split3(path_sub[0], path_sub2, '.');
		msg += "Content-Type: ";
		switch (_type = fileType(path_sub2[path_sub2.size() - 1]))
		{
		case 0:
			msg += "text/html \r\n";
			break;
		case 1:
			msg += "image/jpeg \r\n";
			break;
		case 2:
			msg += "image/gif \r\n";
			break;
		case 3:
			msg += "text/css \r\n";
			break;
		default:
			msg += path_sub2[path_sub2.size() - 1];
			msg += " \r\n";
		}
		int c;
		while ((c = fgetc(pFile)) != EOF) { // standard C I/O file reading loop
			char _c = c;
			filemsg += _c;
			sizeOfFile++;
		}
		fclose(pFile);
		if (filemsg.size() != sizeOfFile)
			std::cout << "File reading caused corruption in the string" << std::endl;
	}
	//if file not found
	else
	{
		msg = "HTTP/1.1 404 Not Found\n";
		while (path_sub.size() < 2)
		{
			path_sub.push_back("");
		}
		if (path_sub[0].size() < 2)
			path_sub[0] = "/index.htm";

		path_sub[0] = "www" + path_sub[0];
		int _type;
		msg += "Content-Type: ";
		msg += "text/html \r\n";
		filemsg += "<html><head><title>Missing File</title></head><body>";
		filemsg += "Request Type: ";
		filemsg += type;
		filemsg += "<br>Path: ";
		filemsg += path_sub[0];
		filemsg += "<br>subdata: ";
		filemsg += path_sub[1];
		filemsg += "</body></html>";
	}
	msg += "Content-Length: ";
	if (sizeOfFile > 0)
		msg += std::to_string(sizeOfFile);
	else
		msg += std::to_string(filemsg.size());
	msg += "\r\n";

	//end header
	msg += "\r\n";
}

void Server::BadReq(std::string& msg, std::string path, std::string type)
{
	std::vector<std::string> path_sub;
	split3(path, path_sub);
	std::string filemsg;
	//try to locate the file
	FILE* pFile = 0;
	int errorNo = (fopen_s(&pFile, path_sub[0].c_str(), "rb"));
	//if file found send file
	long long sizeOfFile = 0;
	msg = "HTTP/1.1 400 Bad Request\n";
	while (path_sub.size() < 2)
	{
		path_sub.push_back("");
	}
	if (path_sub[0].size() < 2)
		path_sub[0] = "/index.htm";

	path_sub[0] = "www" + path_sub[0];
	int _type;
	msg += "Content-Type: ";
	msg += "text/html \r\n";
	filemsg += "<html><head><title>Missing File</title></head><body>";
	filemsg += "Request Type: ";
	filemsg += type;
	filemsg += "<br>Path: ";
	filemsg += path_sub[0];
	filemsg += "<br>subdata: ";
	filemsg += path_sub[1];
	filemsg += "</body></html>";
	msg += "Content-Length: ";
	if (sizeOfFile > 0)
		msg += std::to_string(sizeOfFile);
	else
		msg += std::to_string(filemsg.size());
	msg += "\r\n";

	//end header
	msg += "\r\n";
	for (int i = 0; i < filemsg.size(); i++)
		msg += filemsg.c_str()[i];
}

void Server::Options(std::string& msg, std::string path, std::string type)
{
	std::vector<std::string> path_sub;
	split3(path, path_sub);
	std::string filemsg;
	msg = "HTTP/1.1 204 No Content\n";
	msg += "Allow : OPTIONS, GET, HEAD, POST\n";
	msg += "\r\n";
}

int Server::main()
{
	WSADATA wsaData;
	struct addrinfo hints;
	struct addrinfo *server = NULL;
	SOCKET server_socket = INVALID_SOCKET;
	std::string msg = "";
	std::vector<client_type> client(MAX_CLIENTS);
	int num_clients = 0;
	int temp_id = -1;
	std::thread my_thread[MAX_CLIENTS];

	//Initialize Winsock
	std::cout << "Intializing Winsock..." << std::endl;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	//Setup hints
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	//Setup Server
	std::cout << "Setting up server..." << std::endl;
	getaddrinfo(NULL, DEFAULT_PORT, &hints, &server);

	//Create a listening socket for connecting to server
	std::cout << "Creating server socket..." << std::endl;
	server_socket = socket(server->ai_family, server->ai_socktype, server->ai_protocol);

	//Setup socket options
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &OPTION_VALUE, sizeof(int)); //Make it possible to re-bind to a port that was used within the last 2 minutes
	setsockopt(server_socket, IPPROTO_TCP, TCP_NODELAY, &OPTION_VALUE, sizeof(int)); //Used for interactive programs

																					 //Assign an address to the server socket.
	std::cout << "Binding socket..." << std::endl;
	bind(server_socket, server->ai_addr, (int)server->ai_addrlen);

	//Listen for incoming connections.
	std::cout << "Listening..." << std::endl;
	listen(server_socket, SOMAXCONN);

	//Initialize the client list
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		client[i] = { -1, INVALID_SOCKET };
	}

	while (1)
	{

		SOCKET incoming = INVALID_SOCKET;
		incoming = accept(server_socket, NULL, NULL);

		if (incoming == INVALID_SOCKET) continue;

		//Reset the number of clients
		num_clients = -1;

		//Create a temporary id for the next client
		temp_id = -1;
		for (int i = 0; i < MAX_CLIENTS; i++)
		{
			if (client[i].socket == INVALID_SOCKET && temp_id == -1)
			{
				client[i].socket = incoming;
				client[i].id = i;
				temp_id = i;
			}

			if (client[i].socket != INVALID_SOCKET)
				num_clients++;

			//std::cout << client[i].socket << std::endl;
		}

		if (temp_id != -1)
		{
			//Send the id to that client
			std::cout << "Client #" << client[temp_id].id << " Accepted" << std::endl;

			//Create a thread process for that client
			my_thread[temp_id] = std::thread(process_client, std::ref(client[temp_id]), std::ref(client), std::ref(my_thread[temp_id]));
		}
		else
		{
			msg = "Server is full";
			send(incoming, msg.c_str(), strlen(msg.c_str()), 0);
			std::cout << msg << std::endl;
		}
	} //end while


	  //Close listening socket
	closesocket(server_socket);

	//Close client socket
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		my_thread[i].detach();
		closesocket(client[i].socket);
	}

	//Clean up Winsock
	WSACleanup();
	std::cout << "Program has ended successfully" << std::endl;

	system("pause");
	return 0;
}

void cleanurl(char* str)
{
	char asciinum[3] = { 0 };
	int i = 0, c;

	while (str[i]) {
		if (str[i] == '+')
			str[i] = ' ';
		else if (str[i] == '%') {
			asciinum[0] = str[i + 1];
			asciinum[1] = str[i + 2];
			str[i] = strtol(asciinum, NULL, 16);
			c = i + 1;
			do {
				str[c] = str[c + 2];
			} while (str[2 + (c++)]);
		}
		++i;
	}
}

void cleanurl(std::string& str)
{
	char asciinum[3] = { 0 };
	int i = 0, c;

	while (str.size() > i) {
		if (str[i] == '+')
			str[i] = ' ';
		else if (str[i] == '%') {
			asciinum[0] = str[i + 1];
			asciinum[1] = str[i + 2];
			str[i] = strtol(asciinum, NULL, 16);
			c = i + 1;
			do {
				str[c] = str[c + 2];
			} while (str[2 + (c++)]);
		}
		++i;
	}
}
